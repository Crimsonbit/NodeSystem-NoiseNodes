package at.crimsonbit.nodesystem.node.noise;

import at.crimsonbit.nodesystem.gui.node.GNode;
import at.crimsonbit.nodesystem.node.IGuiNodeType;
import javafx.scene.paint.Color;

public enum Noise implements IGuiNodeType {

	VORONOI("Voronoi-Noise Node"), SIMPLEX("Simplex-Noise Node");

	private String name;

	Noise(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public Class<? extends GNode> getCustomNodeClass() {
		return GNode.class;
	}

	@Override
	public Color getColor() {
		return Color.LIGHTCORAL;
	}

}
